# Angular 1.x

Angular is a client-side JavaScript framework to make HTML interactive.

## Directives
A **Directive** is a marker on a HTML tag that tells AngularJS to run or reference JavaScript code.

```html
<body ng-controller="LoginController">
```
here `ng-controller` is the directive.

### A list of directives in angular
* ng-app
* ng-controller
* ng-show
* ng-hide
* ng-repeat
* ng-bind
* ng-model
* ng-src
* ng-click
* ng-init
* ng-class
* ng-submit
* ng-include

### Custom Directives
**Custom directives** were written to make HTML more expressive. It also improves the readability o f the code.

## Modules
**Modules** are where we write pieces of our AngularJS application. All dependencies are defined in the module.
```js
var app = angular.module('login',[]);
```

## Expressions
**Expressions** allows to insert dynamic values into your HTML. Expressions are surrounded by double curly braces 
```
{{ 4+6 }}
```
this expression evaluvates into `10` under angular.

## Controllers
**Controllers**  define the apps behavior by defining functions and values. Controllers helps get data on to the page.

## Filters
**Filters** are options in expressions to format the data.
```
{{ data | filter:options }}
```

## Services
**Services** gives controller additional functionality, like fetching JSON data from a web service. Angular comes with a bunch of services that can be used to give additional features. 

### List of services in AngularJS
* $http
* $log
* $filter
#### [Back](webtechnologies.md)

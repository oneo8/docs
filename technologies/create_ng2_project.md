# Create Angular 2 Project
## Setup node and npm
Inorder to create and start working on angular project, you might need to install **node** and **npm**. For freebsd install node and npm through package.

```sh
sudo pkg install node
sudo pkg install npm
```
## Install Angular-cli
Now install angular-cli using `npm`
```sh
sudo npm install -g @angular/cli
```
## Create Your App
After installation of @angular/cli and its dependencies, create your webapp
```sh
ng new <new-app-name>
```
## Run your App
```sh
cd <new-app-name>
ng serve
```
Issuing the above two commands, the application would start on a web browser.

#### [Back](angular_2.md)

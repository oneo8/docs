# Installation and configuaration

Following are the installation instructions for PostgreSQL on FreeBSD. At the time of this writing **PostgreSQL96** was available.
    
    sudo pkg install postgresql96-server postgresql96-client

Add postgresql to rc.conf by executing the following line

    sudo sysrc postgresql_enable="YES" 
    
#### [Back](freebsd.md)

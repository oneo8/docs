# Troubleshooting Wireless Networks

Often new **FreeBSD** or **TrueOS** installations have problems with wireless networks. This page talks about some of the common mistakes that were done during installation and ways to rectify it. Also to overcome certain situations where wireless network gets disonnected and the problems surronding crashing of network.

## 1. Check Wireless On/Off
If the wireless adapter was configured properly with the right driver and 
`ifconfig wlan0` results in something similar below, 

```sh
wlan0: flags=8843<UP,BROADCAST,RUNNING,SIMPLEX,MULTICAST> metric 0 mtu 1500
	ether 90:00:4e:3e:2a:18
	hwaddr 90:00:4e:3e:2a:18
	inet 192.168.1.217 netmask 0xffffff00 broadcast 192.168.1.255 
	nd6 options=29<PERFORMNUD,IFDISABLED,AUTO_LINKLOCAL>
	media: IEEE 802.11 Wireless Ethernet autoselect (autoselect)
	status: no carrier
	ssid "" channel 11 (2462 MHz 11g ht/20)
	regdomain FCC country US indoor ecm authmode WPA2/802.11i privacy ON
	deftxkey UNDEF txpower 30 bmiss 7 scanvalid 60 protmode CTS
	ampdulimit 8k ampdudensity 8 shortgi -stbctx stbcrx wme burst
	roaming MANUAL
	groups: wlan 
```
then check if the hardware switch for wireless is turned on on the laptop.

#### [Back](troubleshooting.md)

